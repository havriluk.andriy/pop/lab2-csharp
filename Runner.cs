namespace cnu.andriyh.pop.lab2 {

    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    public class Runner
    {
        private readonly int id;
        private readonly int left;
        private readonly int right;
        private readonly Func<int, int> arrayAccess;
        private readonly Action<int> globalMin;

        public Runner(int id, int left, int right, Func<int, int> arrayAccess, Action<int> globalMin)
        {
            this.id = id;
            this.left = left;
            this.right = right;
            this.arrayAccess = arrayAccess;
            this.globalMin = globalMin;
        }

        public void Run()
        {
            int min = int.MaxValue;
            for (int i = left; i < right; i++)
            {
                min = Math.Min(min, arrayAccess(i));
            }
            globalMin(min);
            Console.WriteLine($"Потік: [{id}] пройшов відрізок [{left} - {right - 1}] та знайшов мінімум:  {min}");
        }
    }
}