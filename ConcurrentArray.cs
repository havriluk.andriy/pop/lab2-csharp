namespace cnu.andriyh.pop.lab2 {

    public class ConcurrentArray
    {
        private readonly int size;
        private readonly List<Runner> runners;
        private readonly int[] _array;
        private int min = int.MaxValue;
        private readonly object lockObj = new object();

        public ConcurrentArray(int size, int threads)
        {
            this.size = size;
            this.runners = CreateThreads(threads);
            _array = new int[size];
            InitArray();
        }

        private void InitArray()
        {
            for (int i = 0; i < size; i++)
            {
                _array[i] = i;
            }

            Random random = new Random();
            int secret = random.Next(size);
            _array[secret] = -1;
            Console.WriteLine("Згенеровано масив з -1 за індексом: " + secret);
        }

        public int Get(int index)
        {
            return _array[index];
        }

        public async Task<int> CalculateMin()
        {
            List<Task> tasks = new List<Task>();
            foreach (var runner in runners)
            {
                Task task = Task.Run(() => runner.Run());
                tasks.Add(task);
            }

            await Task.WhenAll(tasks);
            return min;
        }

        private void ConsumeMin(int min)
        {
            lock (lockObj)
            {
                this.min = Math.Min(min, this.min);
            }
        }

        private List<Runner> CreateThreads(int threads)
        {
            List<Runner> runners = new List<Runner>();
            int left = 0;
            int right = size / threads;

            for (int i = 0; i < threads; i++)
            {
                runners.Add(new Runner(i, left, right, Get, ConsumeMin));
                left = right;
                right = Math.Min(size, right + size / threads);
            }

            return runners;
        }
    }
}