﻿namespace cnu.andriyh.pop.lab2 {

    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Відсутній аргумент програми - кількість потоків.");
                return;
            }

            int threads = int.Parse(args[0]);
            int min = new ConcurrentArray(10_000_000, threads).CalculateMin().Result;
            Console.WriteLine("Глобальний мінімум: " + min);
        }
    }
}